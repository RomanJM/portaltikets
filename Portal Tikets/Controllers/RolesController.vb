﻿Imports System.Web.Mvc

Namespace Controllers
    Public Class RolesController
        Inherits Controller
        Private db As New BaseEntities
        ' GET: Roles
        Function Index() As ActionResult
            If ControlAcceso.ValidarAcceso(Session) = False Then
                Return RedirectToAction("Login", "Acceso")
            End If
            Dim model As List(Of Roles) = Nothing
            model = (From r In db.Roles).ToList()
            Return View(model)
        End Function
        Function Nuevo() As ActionResult
            If ControlAcceso.ValidarAcceso(Session) = False Then
                Return RedirectToAction("Login", "Acceso")
            End If
            Dim model As Roles = New Roles()
            Return View(model)
        End Function
        <HttpPost()>
        Function Nuevo(ByVal model As Roles) As ActionResult
            If ControlAcceso.ValidarAcceso(Session) = False Then
                Return RedirectToAction("Login", "Acceso")
            End If
            Return View(model)
        End Function

        Function Editar(ByVal id As Integer) As ActionResult
            If ControlAcceso.ValidarAcceso(Session) = False Then
                Return RedirectToAction("Login", "Acceso")
            End If
            Dim model As Roles = (From r In db.Roles Where r.IdRol = id).FirstOrDefault
            Return View(model)
        End Function
        <HttpPost()>
        Function Editar(ByVal model As Roles) As ActionResult
            If ControlAcceso.ValidarAcceso(Session) = False Then
                Return RedirectToAction("Login", "Acceso")
            End If
            Return View(model)
        End Function

        Function Eliminar(ByVal id As Integer) As ActionResult
            If ControlAcceso.ValidarAcceso(Session) = False Then
                Return RedirectToAction("Login", "Acceso")
            End If
            Dim model As Roles = (From r In db.Roles Where r.IdRol = id).FirstOrDefault
            Return View(model)
        End Function
        <HttpPost()>
        Function Eliminar(ByVal model As Roles) As ActionResult
            If ControlAcceso.ValidarAcceso(Session) = False Then
                Return RedirectToAction("Login", "Acceso")
            End If
            Return View(model)
        End Function

        Function Detalle(ByVal id As Integer) As ActionResult
            If ControlAcceso.ValidarAcceso(Session) = False Then
                Return RedirectToAction("Login", "Acceso")
            End If
            Dim model As Roles = (From r In db.Roles Where r.IdRol = id).FirstOrDefault
            Return View(model)
        End Function
        <HttpPost()>
        Function Detalle(ByVal model As Roles) As ActionResult
            If ControlAcceso.ValidarAcceso(Session) = False Then
                Return RedirectToAction("Login", "Acceso")
            End If
            Return View(model)
        End Function
    End Class
End Namespace