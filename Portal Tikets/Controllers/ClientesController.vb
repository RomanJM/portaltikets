﻿Imports System.Web.Mvc

Namespace Controllers
    Public Class ClientesController
        Inherits Controller
        Private db As New BaseEntities
        ' GET: Clientes
        Function Index() As ActionResult
            If ControlAcceso.ValidarAcceso(Session) = False Then
                Return RedirectToAction("Login", "Acceso")
            End If
            Dim model As List(Of Clientes) = Nothing
            model = (From r In db.Clientes).ToList()
            Return View(model)
        End Function

        ' GET: Clientes/Details/5
        Function Nuevo() As ActionResult
            Return View()
        End Function

        ' GET: Clientes/Create
        Function Create() As ActionResult
            Return View()
        End Function

        ' POST: Clientes/Create
        <HttpPost()>
        Function Create(ByVal collection As FormCollection) As ActionResult
            Try
                If ControlAcceso.ValidarAcceso(Session) = False Then
                    Return RedirectToAction("Login", "Acceso")
                End If
                ' TODO: Add insert logic here
                Dim cliente As New Clientes With {
                    .Rfc = collection("Rfc"),
                    .RazonSocial = collection("RazonSocial"),
                    .Correo = collection("Correo"),
                    .Direccion = collection("Direccion"),
                    .Telefono = collection("Telefono")
                }
                db.Clientes.Add(cliente)
                db.SaveChanges()
                Return RedirectToAction("Index")
            Catch
                Return View()
            End Try
        End Function

        ' GET: Clientes/Edit/5
        Function Editar(ByVal id As Integer) As ActionResult
            If ControlAcceso.ValidarAcceso(Session) = False Then
                Return RedirectToAction("Login", "Acceso")
            End If
            Dim model As Clientes = (From r In db.Clientes Where r.IdCliente = id).FirstOrDefault
            Return View(model)
        End Function

        ' POST: Clientes/Edit/5
        <HttpPost()>
        Function Editar(ByVal id As Integer, ByVal collection As FormCollection) As ActionResult
            Try
                ' TODO: Add update logic here
                If ControlAcceso.ValidarAcceso(Session) = False Then
                    Return RedirectToAction("Login", "Acceso")
                End If
                Dim model As Clientes = (From r In db.Clientes Where r.IdCliente = id).FirstOrDefault
                model.Rfc = collection("Rfc")
                model.RazonSocial = collection("RazonSocial")
                model.Correo = collection("Correo")
                model.Direccion = collection("Direccion")
                model.Telefono = collection("Telefono")
                db.SaveChanges()
                Return RedirectToAction("Index")
            Catch
                Return View()
            End Try
        End Function

        ' GET: Clientes/Delete/5
        Function Delete(ByVal id As Integer) As ActionResult
            If ControlAcceso.ValidarAcceso(Session) = False Then
                Return RedirectToAction("Login", "Acceso")
            End If
            Dim model As Clientes = (From r In db.Clientes Where r.IdCliente = id).FirstOrDefault
            Return View(model)
        End Function

        ' POST: Clientes/Delete/5
        <HttpPost()>
        Function Delete(ByVal id As Integer, ByVal collection As FormCollection) As ActionResult
            Try
                ' TODO: Add delete logic here

                Return RedirectToAction("Index")
            Catch
                Return View()
            End Try
        End Function
        Function Detalle(ByVal id As Integer) As ActionResult
            If ControlAcceso.ValidarAcceso(Session) = False Then
                Return RedirectToAction("Login", "Acceso")
            End If
            Dim model As Clientes = (From r In db.Clientes Where r.IdCliente = id).FirstOrDefault
            Return View(model)
        End Function
    End Class
End Namespace