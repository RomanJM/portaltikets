﻿Imports System.Web.Mvc
Imports System.Security.Principal
Imports System.Globalization
Imports System.Threading

Namespace Controllers
    Public Class AccesoController
        Inherits Controller
        Private db As New BaseEntities
        <HttpGet()>
        Function Startup() As ActionResult
            Return View()
        End Function

        <HttpGet()>
        Function Login() As ActionResult
            ViewBag.Messages = Nothing
            ViewBag.Idiomadefault = "es"
            Return View(New Accesos())
        End Function

        <HttpPost()>
        Function Login(ByVal model As Accesos) As ActionResult
            ViewBag.Idiomadefault = Request.Form("idioma")
            Try
                Dim PassWord As String = Functions.MD5i2(model.PassWord)
                Dim usuario = (From a In db.Accesos Join u In db.Usuarios On a.IdUsuario Equals u.IdUsuario Where a.ClaveUsuario.Equals(model.ClaveUsuario) And a.PassWord.Equals(PassWord) Select u).FirstOrDefault()
                If IsNothing(usuario) Then
                    ViewBag.Messages = My.Resources.Login.MSG_ERROR_ACCESO
                    Return View(model)
                End If
                Dim parametro As Parametros = (From p In db.Parametros Where p.Clave.Equals("TIMEOUTSESSION")).FirstOrDefault()
                Session.Timeout = IIf(IsNothing(parametro), 20, Convert.ToInt32(parametro.Valor))
                Session("Usuario") = usuario
                Return RedirectToAction("Index", "Home")
            Catch ex As Exception
                ViewBag.Messages = My.Resources.Login.MSG_ERROR_ACCESO
                Return View(model)
            End Try
        End Function
        <HttpGet()>
        Function Logout() As ActionResult
            Session("Usuario") = Nothing
            Return RedirectToAction("Login", "Acceso")
        End Function

        Function Idioma(ByVal l As String) As ActionResult
            If l IsNot Nothing Then
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(l)
                Thread.CurrentThread.CurrentUICulture = New CultureInfo(l)
            End If

            Dim cookie As HttpCookie = New HttpCookie("Language") With {
                .Value = l
            }
            Response.Cookies.Add(cookie)
            ViewBag.Messages = Nothing
            ViewBag.Idiomadefault = l
            Return View("Login", New Accesos())
        End Function
    End Class
End Namespace