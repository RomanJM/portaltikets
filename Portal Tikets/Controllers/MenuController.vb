﻿Imports System.Web.Mvc

Namespace Controllers
    Public Class MenuController
        Inherits Controller
        Private db As New BaseEntities
        ' GET: Menu
        Function Index() As ActionResult
            If ControlAcceso.ValidarAcceso(Session) = False Then
                Return RedirectToAction("Login", "Acceso")
            End If
            Dim model As List(Of Menu) = Nothing
            model = (From r In db.Menu).ToList()
            Return View(model)
        End Function
        Function Nuevo() As ActionResult
            If ControlAcceso.ValidarAcceso(Session) = False Then
                Return RedirectToAction("Login", "Acceso")
            End If
            Dim model As Menu = New Menu()
            Return View(model)
        End Function
        <HttpPost()>
        Function Nuevo(ByVal model As Menu) As ActionResult
            If ControlAcceso.ValidarAcceso(Session) = False Then
                Return RedirectToAction("Login", "Acceso")
            End If
            If (ModelState.IsValid = False) Then
                Return View()
            End If
            db.Menu.Add(model)
            db.SaveChanges()
            Dim menus As List(Of Menu) = (From r In db.Menu).ToList()
            Return View("Index", menus)
        End Function
    End Class
End Namespace