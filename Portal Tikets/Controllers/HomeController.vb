﻿Public Class HomeController
    Inherits Controller
    Private db As New BaseEntities
    Function Index() As ActionResult
        If ControlAcceso.ValidarAcceso(Session) = False Then
            Return RedirectToAction("Login", "Acceso")
        End If
        Dim usuario As Usuarios = Session("Usuario")
        If IsNothing(usuario) Then
            Return RedirectToAction("Login", "Acceso")
        End If
        Dim usuariosRoles As ICollection(Of UsuariosRoles) = usuario.UsuariosRoles
        Dim roles() = usuariosRoles.Select(Function(x) x.IdRol).ToArray()
        If roles.Length = 0 Then
            'Usuario no tiene establecido ningun rol
        End If
        'Se obtiene los menus
        Dim menuRoles As List(Of MenuRoles) = (From m In db.MenuRoles Where roles.Contains(m.IdRol)).ToList()
        Dim menus() = menuRoles.Select(Function(x) x.IdMenu).ToArray()
        Dim menusUser As List(Of Menu) = (From m In db.Menu Where menus.Contains(m.IdMenu)).ToList()

        'Se obtiene los submenus
        Dim SubmenuRoles As List(Of SubMenuRoles) = (From m In db.SubMenuRoles Where roles.Contains(m.IdRol)).ToList()
        Dim submenus() = SubmenuRoles.Select(Function(x) x.IdSubMenu).ToArray()
        For Each menu As Menu In menusUser
            menu.SubMenu = menu.SubMenu.Where(Function(x) submenus.Contains(x.IdSubMenu)).ToList()
        Next
        Session("MenuUser") = menusUser
        Return View()
    End Function
End Class
