﻿Imports System.Security.Cryptography

Public Class Functions
    Public Shared Function Reverse(ByVal s As String) As String
        Dim charArray() As Char = s.ToCharArray()
        Array.Reverse(charArray)
        Return New String(charArray)
    End Function

    Public Shared Function MD5i2(ByVal str As String) As String
        Dim md5i As String = MD5(str)
        md5i = MD5(Reverse(md5i))
        Return Reverse(md5i)
    End Function

    Public Shared Function MD5(ByVal str As String) As String
        Try
            Dim mmd5 As MD5 = MD5CryptoServiceProvider.Create()
            Dim encoding As ASCIIEncoding = New ASCIIEncoding()
            Dim stream() As Byte = Nothing
            Dim sb As StringBuilder = New StringBuilder()
            stream = mmd5.ComputeHash(encoding.GetBytes(str))
            For i As Integer = 0 To stream.Length - 1
                sb.AppendFormat("{0:x2}", stream(i))
            Next
            Return sb.ToString()
        Catch ex As Exception
            Return ""
        End Try
    End Function
End Class
