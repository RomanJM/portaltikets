﻿@Code
    Dim menu As ICollection(Of Menu) = Session("MenuUser")
    Dim menuTraduccion = Portal_Tikets.My.Resources.Menu.ResourceManager
    Dim submenuTraduccion = Portal_Tikets.My.Resources.SubMenu.ResourceManager

End Code

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@ViewBag.Title - Portal Tickets</title>
    @Scripts.Render("~/bundles/jquery")
    @Styles.Render("~/Content/css")
    @Scripts.Render("~/bundles/modernizr")
</head>
<body class="fondo">
    <nav class="navbar fixed-top navbar-inverse navbar-toggleable-md">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse navbar-right" id="navbarTogglerDemo01">
            <a class="navbar-brand" href="#">
                <img src="https://i.ytimg.com/vi/GEUgclDvO24/maxresdefault.jpg" height="40" width="100" />
            </a>
            <ul class="navbar-nav mr-auto">
                @For Each m In menu
                    @<text>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @menuTraduccion.GetString(m.Traduccion)
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                @For Each s In m.SubMenu
                                    @Html.ActionLink(submenuTraduccion.GetString(s.Traduccion), s.ActionName, s.ControllerName, New With {.area = ""}, New With {.class = "dropdown-item"})
                                Next
                            </div>
                        </li>
                    </text>
                Next
                <li class="nav-item">@Html.ActionLink("Salir", "Logout", "Acceso", New With {.area = ""}, New With {.class = "nav-link"})</li>
            </ul>
        </div>
    </nav>

    <div class="container body-content" style="font-size:.8rem">
        <br />
        @RenderBody()
        <hr />
        <footer>
            <p>&copy; @DateTime.Now.Year - Portal Tikets - Consultoria en desarrollo puntual</p>
        </footer>
    </div>
    @RenderSection("scripts", required:=False)
    @Scripts.Render("~/bundles/bootstrap")
    @Scripts.Render("~/bundles/iziModal")
    @Styles.Render("~/iziModalCss/iziModal")
</body>
</html>
