﻿@ModelType IEnumerable(Of Portal_Tikets.Clientes)
@Code
    ViewData("Title") = "Clientes"
End Code
<script>
    $(document).ready(function () {
        $("#modalConfirmacion").iziModal({
            width: 300,
            overlayClose: false,
            autoOpen: false,
            title : "Confirmacion"
        });
    });
    
    function Nuevo() {
        window.location.replace("@Url.Content("~/Clientes/Create")");
    }
    function Editar(id) {
        window.location.replace("@Url.Content("~/Clientes/Editar")?id="+id);
    }
    function Eliminar() {
        $("#modalConfirmacion").iziModal("open");
    }
    function CerrarModal() {
        $("#modalConfirmacion").iziModal("close");
    }
    function Detalle(id) {
        window.location.replace("@Url.Content("~/Clientes/Detalle")?id="+id);
    }
</script>
<div class="card card-inverse card-primary fondo-transparent">
    <div class="card-header">
        Lista de clientes
        <button class="btn btn-sm btn-success fondo-transparent text-success" onclick="Nuevo()" style="float:right">
            <i class="fas fa-plus-square"></i> Nuevo
        </button>
    </div>
    <div class="card-block">
        <table class="table table-striped table-hover table-sm">
            <tr class="text-info">
                <th class=" text-center">
                    @Html.DisplayNameFor(Function(model) model.Rfc)
                </th>
                <th class=" text-center">
                    Razon Social
                </th>
                <th class=" text-center">
                    @Html.DisplayNameFor(Function(model) model.Correo)
                </th>
                <th class=" text-center">
                    @Html.DisplayNameFor(Function(model) model.Direccion)
                </th>
                <th class=" text-center">
                    @Html.DisplayNameFor(Function(model) model.Telefono)
                </th>                
                <th></th>
            </tr>

            @For Each item In Model
                @<tr style="text-align:center">
                    <td>
                        @Html.DisplayFor(Function(modelItem) item.Rfc)
                    </td>
                    <td>
                        @Html.DisplayFor(Function(modelItem) item.RazonSocial)
                    </td>
                    <td>
                        @Html.DisplayFor(Function(modelItem) item.Correo)
                    </td>
                    <td>
                        @Html.DisplayFor(Function(modelItem) item.Direccion)
                    </td>
                    <td>
                        @Html.DisplayFor(Function(modelItem) item.Telefono)
                    </td>                  
                    <td>
                        <button class="btn btn-sm btn-warning text-warning fondo-transparent" onclick="Editar(@item.IdCliente)">
                            <i class="fas fa-edit"></i>
                        </button>
                        <button class="btn btn-sm btn-info text-info fondo-transparent" onclick="Detalle(@item.IdCliente )">
                            <i class="fas fa-info-circle"></i>
                        </button>
                        <button class="btn btn-sm btn-danger text-danger fondo-transparent" onclick="Eliminar(@item.IdCliente )">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                    </td>
                </tr>
            Next

        </table>
    </div>
</div>
<div id="modalConfirmacion" style="display:none">
    <h4 style="color:black">¿Esta seguro que desea eliminar el registro?</h4>
    <button class="btn btn-sm btn-danger text-danger fondo-transparent" onclick="CerrarModal()">
        Cancelar
    </button>
    <button class="btn btn-sm btn-success text-success fondo-transparent" onclick="CerrarModal()">
        Aceptar
    </button>
</div>