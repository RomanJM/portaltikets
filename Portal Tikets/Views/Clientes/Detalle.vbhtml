﻿@ModelType Portal_Tikets.Clientes
@Code
    ViewData("Title") = "Detalle"
End Code

<h2>Detalle</h2>

<div>
    <h4>Clientes</h4>
    <hr />
    <dl class="dl-horizontal">
        <dt>
            @Html.DisplayNameFor(Function(model) model.Rfc)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Rfc)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.RazonSocial)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.RazonSocial)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Correo)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Correo)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Direccion)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Direccion)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Telefono)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Telefono)
        </dd>

    </dl>
</div>
<p>
    @Html.ActionLink("Editar", "Editar", New With {.id = Model.IdCliente}) |
    @Html.ActionLink("Back to List", "Index")
</p>
