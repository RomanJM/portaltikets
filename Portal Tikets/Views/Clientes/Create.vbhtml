﻿@ModelType Portal_Tikets.Clientes
@Code
    ViewData("Title") = "Nuevo"
End Code

<h2>Create</h2>

@Using (Html.BeginForm()) 
    @Html.AntiForgeryToken()
    
    @<div class="form-horizontal">
        <h4>Clientes</h4>
        <hr />
        @Html.ValidationSummary(True, "", New With { .class = "text-danger" })
        <div class="form-group">
            @Html.LabelFor(Function(model) model.Rfc, htmlAttributes:= New With { .class = "control-label col-md-2" })
            <div class="col-md-10">
                @Html.EditorFor(Function(model) model.Rfc, New With {.htmlAttributes = New With {.class = "form-control fondo-transparent"}})
                @Html.ValidationMessageFor(Function(model) model.Rfc, "", New With { .class = "text-danger" })
            </div>
        </div>

        <div class="form-group">
            @Html.LabelFor(Function(model) model.RazonSocial, htmlAttributes:= New With { .class = "control-label col-md-2" })
            <div class="col-md-10">
                @Html.EditorFor(Function(model) model.RazonSocial, New With {.htmlAttributes = New With {.class = "form-control fondo-transparent"}})
                @Html.ValidationMessageFor(Function(model) model.RazonSocial, "", New With { .class = "text-danger" })
            </div>
        </div>

        <div class="form-group">
            @Html.LabelFor(Function(model) model.Correo, htmlAttributes:= New With { .class = "control-label col-md-2" })
            <div class="col-md-10">
                @Html.EditorFor(Function(model) model.Correo, New With {.htmlAttributes = New With {.class = "form-control fondo-transparent"}})
                @Html.ValidationMessageFor(Function(model) model.Correo, "", New With { .class = "text-danger" })
            </div>
        </div>

        <div class="form-group">
            @Html.LabelFor(Function(model) model.Direccion, htmlAttributes:= New With { .class = "control-label col-md-2" })
            <div class="col-md-10">
                @Html.EditorFor(Function(model) model.Direccion, New With {.htmlAttributes = New With {.class = "form-control fondo-transparent"}})
                @Html.ValidationMessageFor(Function(model) model.Direccion, "", New With { .class = "text-danger" })
            </div>
        </div>

        <div class="form-group">
            @Html.LabelFor(Function(model) model.Telefono, htmlAttributes:= New With { .class = "control-label col-md-2" })
            <div class="col-md-10">
                @Html.EditorFor(Function(model) model.Telefono, New With {.htmlAttributes = New With {.class = "form-control fondo-transparent"}})
                @Html.ValidationMessageFor(Function(model) model.Telefono, "", New With { .class = "text-danger" })
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <input type="submit" value="Crear" class="btn btn-primary" />
            </div>
        </div>
    </div>
End Using

     <div>
         <i class="fas fa-chevron-circle-left"></i>  @Html.ActionLink("Regresar a la lista", "Index")
     </div>
