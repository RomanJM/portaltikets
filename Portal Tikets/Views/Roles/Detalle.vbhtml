﻿@ModelType Portal_Tikets.Roles
@Code
    ViewData("Title") = "Detalle"
End Code

<h2>Detalle</h2>

<div>
    <h4>Roles</h4>
    <hr />
    <dl class="dl-horizontal">
        <dt>
            @Html.DisplayNameFor(Function(model) model.Clave)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Clave)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Descripcion)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Descripcion)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Estatus)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Estatus)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.ClaveTraduccion)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.ClaveTraduccion)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.FechaCreacion)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.FechaCreacion)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Usuarios.Nombre)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Usuarios.Nombre)
        </dd>

    </dl>
</div>
<p>
    @Html.ActionLink("Edit", "Edit", New With { .id = Model.IdRol }) |
    @Html.ActionLink("Back to List", "Index")
</p>
