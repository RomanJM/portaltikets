﻿@ModelType Portal_Tikets.Roles
@Code
    ViewData("Title") = "Eliminar"
End Code

<h2>Eliminar</h2>

<h3>Are you sure you want to delete this?</h3>
<div>
    <h4>Roles</h4>
    <hr />
    <dl class="dl-horizontal">
        <dt>
            @Html.DisplayNameFor(Function(model) model.Clave)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Clave)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Descripcion)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Descripcion)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Estatus)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Estatus)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.ClaveTraduccion)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.ClaveTraduccion)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.FechaCreacion)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.FechaCreacion)
        </dd>

        <dt>
            @Html.DisplayNameFor(Function(model) model.Usuarios.Nombre)
        </dt>

        <dd>
            @Html.DisplayFor(Function(model) model.Usuarios.Nombre)
        </dd>

    </dl>
    @Using (Html.BeginForm())
        @Html.AntiForgeryToken()

        @<div class="form-actions no-color">
            <input type="submit" value="Delete" class="btn btn-default" /> |
            @Html.ActionLink("Back to List", "Index")
        </div>
    End Using
</div>
