﻿@ModelType IEnumerable(Of Portal_Tikets.Roles)
@Code
    ViewData("Title") = "Roles"
End Code
<br />
<script>
    function Nuevo() {
        window.location.replace("@Url.Content("~/Roles/Nuevo")");
    }
    function Editar(id) {
        window.location.replace("@Url.Content("~/Roles/Editar")?id="+id);
    }
    function Eliminar(id) {
        window.location.replace("@Url.Content("~/Roles/Eliminar")?id="+id);
    }
    function Detalle(id) {
        window.location.replace("@Url.Content("~/Roles/Detalle")?id="+id);
    }
</script>
<div class="card card-inverse card-primary fondo-transparent" >
    <div class="card-header">
        Lista de roles
        <button class="btn btn-sm btn-success fondo-transparent text-success" onclick="Nuevo()" style="float:right">
            <i class="fas fa-plus-square"></i> Nuevo
        </button>
    </div>
    <div class="card-block">
        <table class="table table-striped table-hover table-sm">
            <tr class="text-info">
                <th>
                    @Html.DisplayNameFor(Function(model) model.Clave)
                </th>
                <th>
                    @Html.DisplayNameFor(Function(model) model.Descripcion)
                </th>
                <th>
                    @Html.DisplayNameFor(Function(model) model.Estatus)
                </th>
                <th>
                    @Html.DisplayNameFor(Function(model) model.ClaveTraduccion)
                </th>
                <th>
                    @Html.DisplayNameFor(Function(model) model.FechaCreacion)
                </th>
                <th>
                    @Html.DisplayNameFor(Function(model) model.Usuarios.Nombre)
                </th>
                <th></th>
            </tr>

            @For Each item In Model
                @<tr>
                    <td>
                        @Html.DisplayFor(Function(modelItem) item.Clave)
                    </td>
                    <td>
                        @Html.DisplayFor(Function(modelItem) item.Descripcion)
                    </td>
                    <td>
                        @Html.DisplayFor(Function(modelItem) item.Estatus)
                    </td>
                    <td>
                        @Html.DisplayFor(Function(modelItem) item.ClaveTraduccion)
                    </td>
                    <td>
                        @Html.DisplayFor(Function(modelItem) item.FechaCreacion)
                    </td>
                    <td>
                        @Html.DisplayFor(Function(modelItem) item.Usuarios.Nombre)
                    </td>
                    <td>
                        <button class="btn btn-sm btn-warning text-warning fondo-transparent" onclick="Editar(@item.IdRol)">
                            <i class="fas fa-edit"></i>
                        </button>
                        <button class="btn btn-sm btn-info text-info fondo-transparent" onclick="Detalle(@item.IdRol)">
                            <i class="fas fa-info-circle"></i>
                        </button>
                        <button class="btn btn-sm btn-danger text-danger fondo-transparent" onclick="Eliminar(@item.IdRol)">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                    </td>
                </tr>
            Next

        </table>
    </div>
</div>

