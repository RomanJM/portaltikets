﻿@Code
    ViewData("Title") = "Startup"
End Code

<script>
    function LogIn() {
        window.location.replace("@Url.Content("~/Acceso/Login")");
    }
    function Ticket() {
        window.location.replace("@Url.Content("~/Ticket/Alta")");
    }
</script>

<div style="background-color: transparent; width:30%;margin: 5% 35%">
    <img src="https://i.ytimg.com/vi/GEUgclDvO24/maxresdefault.jpg" style="width:100%; height:100%" />
</div>
<button class="btn btn-primary fondo-transparent" onclick="LogIn()">
    <i class="fas fa-sign-in-alt"></i> Iniciar Sesion
</button>
<button class="btn btn-success fondo-transparent" onclick="Ticket()">
    <i class="fas fa-list-alt"></i> Levantar Ticket
</button>

