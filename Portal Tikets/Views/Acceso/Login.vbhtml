﻿@ModelType Portal_Tikets.Accesos
@Code
    Dim traduccion = Portal_Tikets.My.Resources.Login.ResourceManager
    ViewData("Title") = "Log In"
    Dim activeEs As String = "active"
    Dim chekedEs As String = "checked"
    Dim activeEn As String = ""
    Dim chekedEn As String = ""
    If ViewBag.Idiomadefault.Equals("en") Then
        activeEs = ""
        chekedEs = ""
        activeEn = "active"
        chekedEn = "checked"
    End If
End Code

<script>
    function Change(language) {
        window.location.href = "@Url.Content("~/Acceso/Idioma")?l=" + language;
    }
</script>
@Using (Html.BeginForm(actionName:="Login", controllerName:="Acceso", method:=FormMethod.Post))
    @Html.AntiForgeryToken()
    @<text>
        <div class="btn-group btn-group-toggle" data-toggle="buttons">
            <label class="btn btn-sm btn-success @activeEs">
                <input type="radio" name="idioma" id="idioma1" autocomplete="off" onchange="Change('es')" value="es" @chekedEs> @traduccion.GetString("IDIOMA_ES")
            </label>
            <label class="btn btn-sm btn-success @activeEn">
                <input type="radio" name="idioma" id="idioma2" autocomplete="off" onchange="Change('en')" value="en" @chekedEn> @traduccion.GetString("IDIOMA_EN")
            </label>
        </div>
        @If ViewBag.Messages <> Nothing Then
            @<div class="alert alert-warning" role="alert" style="width:30%;margin: 0 35%">@ViewBag.Messages</div>
        End If
        <div class="card card-inverse card-primary fondo-transparent" style="width:30%;margin: 5% 35%">
            <div class="card-header">
                @traduccion.GetString("TITLE_PAGE")
            </div>
            <div class="card-block">
                <div class="form-group">
                    <label for="user">@traduccion.GetString("LABEL_USER")</label>
                    <div class="input-group input-group-sm">
                        <span class="input-group-addon" id="basic-addon1"><i class="fas fa-user"></i></span>
                        <input type="text" class="form-control input-border" value="@Model.ClaveUsuario" id="ClaveUsuario" name="ClaveUsuario" />
                    </div>
                </div>
                <div Class="form-group">
                    <Label for="password">@traduccion.GetString("LABEL_PASSWORD")</Label>
                    <div class="input-group input-group-sm">
                        <span class="input-group-addon" id="basic-addon1"><i class="fas fa-key"></i></span>
                        <input type="password" class="form-control input-border" value="@Model.PassWord" id="PassWord" name="PassWord" />
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-sm btn-success fondo-transparent" type="submit">
                    <i class="fas fa-sign-in-alt"></i> @traduccion.GetString("BTN_INICIAR")
                </button>
            </div>
        </div>
    </text>
End Using

