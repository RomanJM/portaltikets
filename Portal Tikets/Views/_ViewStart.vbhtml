﻿@Code
    If (IsNothing(Session("Usuario"))) Then
        Layout = "~/Views/Shared/_Acceso.vbhtml"
    Else
        Layout = "~/Views/Shared/_Layout.vbhtml"
    End If

End Code