﻿@ModelType IEnumerable(Of Portal_Tikets.Menu)
@Code
    ViewData("Title") = "Menu"
End Code
<br />
<script>
    function Nuevo() {
        window.location.replace("@Url.Content("~/Menu/Nuevo")");
    }
    function Editar(id) {
        window.location.replace("@Url.Content("~/Menu/Editar")?id="+id);
    }
    function Eliminar(id) {
        window.location.replace("@Url.Content("~/Menu/Eliminar")?id="+id);
    }
    function Detalle(id) {
        window.location.replace("@Url.Content("~/Menu/Detalle")?id="+id);
    }
</script>
<div class="card card-inverse card-primary fondo-transparent">
    <div class="card-header">
        Lista de menus
        <button class="btn btn-sm btn-success fondo-transparent text-success" onclick="Nuevo()" style="float:right">
            <i class="fas fa-plus-square"></i> Nuevo
        </button>
    </div>
    <div class="card-block">
        <table class="table table-striped table-hover table-sm">
            <tr class="text-info">
                <th>
                    @Html.DisplayNameFor(Function(model) model.Descripcion)
                </th>
                <th>
                    @Html.DisplayNameFor(Function(model) model.Icon)
                </th>
                <th>
                    @Html.DisplayNameFor(Function(model) model.Traduccion)
                </th>
                <th>
                    @Html.DisplayNameFor(Function(model) model.Estatus)
                </th>
                <th></th>
            </tr>

            @For Each item In Model
                @<tr>
                    <td>
                        @Html.DisplayFor(Function(modelItem) item.Descripcion)
                    </td>
                    <td>
                        @Html.DisplayFor(Function(modelItem) item.Icon)
                    </td>
                    <td>
                        @Html.DisplayFor(Function(modelItem) item.Traduccion)
                    </td>
                    <td>
                        @Html.DisplayFor(Function(modelItem) item.Estatus)
                    </td>
                    <td>
                        <button class="btn btn-sm btn-warning text-warning fondo-transparent" onclick="Editar(@item.IdMenu )">
                            <i class="fas fa-edit"></i>
                        </button>
                        <button class="btn btn-sm btn-info text-info fondo-transparent" onclick="Detalle(@item.IdMenu)">
                            <i class="fas fa-info-circle"></i>
                        </button>
                        <button class="btn btn-sm btn-danger text-danger fondo-transparent" onclick="Eliminar(@item.IdMenu)">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                    </td>
                </tr>
            Next

        </table>
    </div>
</div>