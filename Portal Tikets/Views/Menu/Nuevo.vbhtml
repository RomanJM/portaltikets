﻿@ModelType Portal_Tikets.Menu
@Code
    ViewData("Title") = "Nuevo"
End Code

@Using (Html.BeginForm())
    @Html.AntiForgeryToken()
    @<text>
        <div class="card card-inverse card-primary fondo-transparent" >
            <div class="card-header text-center">
                Agregar menu
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col col-sm-6 col-md-6 ">
                        <div class="form-group">
                            @Html.LabelFor(Function(model) model.Descripcion, htmlAttributes:=New With {.class = "control-label"})
                            @Html.EditorFor(Function(model) model.Descripcion, New With {.htmlAttributes = New With {.class = "form-control form-control-sm"}})
                            @Html.ValidationMessageFor(Function(model) model.Descripcion, "", New With {.class = "text-danger"})
                        </div>
                    </div>
                    <div class="col col-sm-6 col-md-6 ">
                        <div class="form-group">
                            @Html.LabelFor(Function(model) model.Icon, htmlAttributes:=New With {.class = "control-label"})
                            @Html.EditorFor(Function(model) model.Icon, New With {.htmlAttributes = New With {.class = "form-control form-control-sm"}})
                            @Html.ValidationMessageFor(Function(model) model.Icon, "", New With {.class = "text-danger"})
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-sm-6 col-md-6 ">
                        <div class="form-group">
                            @Html.LabelFor(Function(model) model.Traduccion, htmlAttributes:=New With {.class = "control-label"})
                            @Html.EditorFor(Function(model) model.Traduccion, New With {.htmlAttributes = New With {.class = "form-control form-control-sm"}})
                            @Html.ValidationMessageFor(Function(model) model.Traduccion, "", New With {.class = "text-danger"})
                        </div>
                    </div>
                    <div class="col col-sm-6 col-md-6 ">
                        <div class="form-group ">
                            @Html.LabelFor(Function(model) model.Estatus, htmlAttributes:=New With {.class = "control-label"})
                            <div class="checkbox">
                                @Html.EditorFor(Function(model) model.Estatus)
                                @Html.ValidationMessageFor(Function(model) model.Estatus, "", New With {.class = "text-danger"})
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-center">
                <button type="submit" class="btn btn-sm btn-success fondo-transparent text-success">
                    <i class="fas fa-plus-square"></i> Guardar
                </button>
            </div>
        </div>
    </text>

End Using
