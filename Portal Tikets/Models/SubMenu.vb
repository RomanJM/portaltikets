'------------------------------------------------------------------------------
' <auto-generated>
'     Este código se generó a partir de una plantilla.
'
'     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
'     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class SubMenu
    Public Property IdSubMenu As Integer
    Public Property IdMenu As Integer
    Public Property Descripcion As String
    Public Property Icon As String
    Public Property Traduccion As String
    Public Property Estatus As Boolean
    Public Property ActionName As String
    Public Property ControllerName As String

    Public Overridable Property Menu As Menu
    Public Overridable Property SubMenuRoles As ICollection(Of SubMenuRoles) = New HashSet(Of SubMenuRoles)

End Class
