'------------------------------------------------------------------------------
' <auto-generated>
'     Este código se generó a partir de una plantilla.
'
'     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
'     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations

Partial Public Class Menu
    Public Property IdMenu As Integer
    <Required(ErrorMessageResourceName:="VALIDA_DESCRIPCION", ErrorMessageResourceType:=GetType(My.Resources.Menu))>
    Public Property Descripcion As String
    <Required(ErrorMessageResourceName:="VALIDA_ICON", ErrorMessageResourceType:=GetType(My.Resources.Menu))>
    Public Property Icon As String
    <Required(ErrorMessageResourceName:="VALIDA_TRADUCCION", ErrorMessageResourceType:=GetType(My.Resources.Menu))>
    Public Property Traduccion As String
    Public Property Estatus As Boolean

    Public Overridable Property SubMenu As ICollection(Of SubMenu) = New HashSet(Of SubMenu)
    Public Overridable Property MenuRoles As ICollection(Of MenuRoles) = New HashSet(Of MenuRoles)

End Class
