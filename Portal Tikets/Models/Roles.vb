'------------------------------------------------------------------------------
' <auto-generated>
'     Este código se generó a partir de una plantilla.
'
'     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
'     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Collections.Generic

Partial Public Class Roles
    Public Property IdRol As Integer
    Public Property Clave As String
    Public Property Descripcion As String
    Public Property Estatus As Boolean
    Public Property ClaveTraduccion As String
    Public Property FechaCreacion As Date
    Public Property IdUsuarioCreacion As Integer

    Public Overridable Property Usuarios As Usuarios
    Public Overridable Property UsuariosRoles As ICollection(Of UsuariosRoles) = New HashSet(Of UsuariosRoles)
    Public Overridable Property MenuRoles As ICollection(Of MenuRoles) = New HashSet(Of MenuRoles)
    Public Overridable Property SubMenuRoles As ICollection(Of SubMenuRoles) = New HashSet(Of SubMenuRoles)

End Class
