﻿Imports System.Globalization
Imports System.Threading
Imports System.Web.Optimization

Public Class MvcApplication
    Inherits System.Web.HttpApplication

    Protected Sub Application_Start()
        AreaRegistration.RegisterAllAreas()
        FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters)
        RouteConfig.RegisterRoutes(RouteTable.Routes)
        BundleConfig.RegisterBundles(BundleTable.Bundles)
    End Sub

    Protected Sub Application_BeginRequest()
        Dim cookie As HttpCookie = HttpContext.Current.Request.Cookies("Language")
        If IsNothing(cookie) Then
            Thread.CurrentThread.CurrentCulture = New CultureInfo("es")
            Thread.CurrentThread.CurrentUICulture = New CultureInfo("es")
        Else
            Thread.CurrentThread.CurrentCulture = New CultureInfo(cookie.Value)
            Thread.CurrentThread.CurrentUICulture = New CultureInfo(cookie.Value)
        End If
    End Sub
End Class
